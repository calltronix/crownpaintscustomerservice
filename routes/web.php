<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('home','HomeController@index')->name('home');
Route::get(' auth/join/team/{id}', 'JoinTeamController@index');
Route::post(' auth/complete-profile', 'JoinTeamController@completeProfile');
Route::group(['middleware'=>['auth','web','verified','member']],function(){
    Route::get('home','HomeController@index')->middleware('verified');
    Route::get('verifyphone','HomeController@verifyphone')->middleware('verified');
    Route::get('resendphonecode','HomeController@resendphonecode')->middleware('verified');
    Route::post('requestphoneverification','HomeController@requestphoneverification')->middleware('verified');

    $routes_path = base_path('routes');
    $route_files = File::allFiles(base_path('routes'));
    foreach($route_files as $file){
        $path = $file->getPath();
        if($path != $routes_path){
            $file_name = $file->getFileName();
            $prefix = str_replace($file_name,'',$path);
            $prefix = str_replace($routes_path,'',$prefix);
            $file_path = $file->getPathName();
            $this->route_path = $file_path;
            $arr = explode('/',$prefix);
            $len = count($arr);
            $main_file= $arr[$len-1];
            $arr = array_map('ucwords',$arr);
            $arr = array_filter($arr);
            $ext_route = str_replace('app.route.php','',$file_name);
            $ext_route = str_replace($main_file,'',$ext_route);
            $ext_route = str_replace('.route.php','',$ext_route);
            $ext_route = str_replace('web','',$ext_route);
            if($ext_route)
                $ext_route = '/'.$ext_route;
            Route::group(['namespace'=>implode('\\',$arr),'prefix'=>strtolower($prefix.$ext_route)],function(){
                require $this->route_path;
            });
        }
    }
});

Auth::routes(['verify' => true]);

