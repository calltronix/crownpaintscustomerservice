<?php
$controller = "PropertyCategoriesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storePropertyCategory');
Route::get('/list',$controller.'listPropertyCategories');
Route::delete('/delete/{propertycategory}',$controller.'destroyPropertyCategory');