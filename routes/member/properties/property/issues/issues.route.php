<?php
$controller = "IssuesController@";
Route::get('{property_id}',$controller.'index');
Route::post('/',$controller.'storeIssue');
Route::post('resolve/{issue_id}',$controller.'resolveIssue');
Route::get('/list/{property_id}',$controller.'listIssues');
Route::get('/list/pending/{property_id}',$controller.'listPendingIssues');
Route::get('/list/resolved/{property_id}',$controller.'listResolvedIssues');
Route::get('/details/{issue_id}',$controller.'issueDetails');
Route::delete('/delete/{issue}',$controller.'destroyIssue');