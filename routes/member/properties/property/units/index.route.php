<?php
$controller = "UnitsController@";
Route::get('{id}/new',$controller.'newUnits');
Route::get('/',$controller.'index');

Route::post('remove',$controller.'updateSessIdUnitsCount');
Route::get('add',$controller.'addSessIdUnitsCount');
Route::get('units-upload/{id}',$controller.'uploadUnitsView');
Route::post('units-upload',$controller.'storeUploadedUnits');
Route::get('units-upload/list/{id}',$controller.'getImportedFileContent');
Route::post('/',$controller.'newUnit');
Route::post('units-file',$controller.'uploadUnitsFile');
Route::post('edit',$controller.'updateUnit');
Route::post('save',$controller.'storeUnits');
Route::get('/list/{property_id}',$controller.'listUnits');
Route::get('/list/unoccupied/{property_id}',$controller.'listOccupiedUnits');

Route::delete('/delete/{unit}',$controller.'destroyUnit');