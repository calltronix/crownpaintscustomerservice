<?php
$controller = "PropertyController@";
Route::get('/{id}',$controller.'index');
Route::post('/',$controller.'store');
Route::get('/list',$controller.'list');
Route::get('{id}/count',$controller.'countTabs');
Route::get('/tenants/{id}',$controller.'listTenants');