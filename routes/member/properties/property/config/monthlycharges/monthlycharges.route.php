<?php
$controller = "MonthlyChargesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeMonthlyCharges');
Route::get('/list/{id}',$controller.'listMonthlyCharges');
Route::post('/activate/{id}',$controller.'activate');
Route::post('/deactivate/{id}',$controller.'deactivate');