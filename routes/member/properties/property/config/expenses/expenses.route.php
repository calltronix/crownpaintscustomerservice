<?php
$controller = "RecurringExpensesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeRecurringExpenses');
Route::get('/list/{id}',$controller.'listRecurringExpenses');
Route::post('/activate/{id}',$controller.'activate');
Route::post('/deactivate/{id}',$controller.'deactivate');