<?php
$controller = "PenaltyConfigController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storePenalty');
Route::get('/list/{id}',$controller.'listPenalty');
Route::post('/activate/{id}',$controller.'activate');
Route::post('/deactivate/{id}',$controller.'deactivate');