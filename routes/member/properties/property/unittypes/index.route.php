<?php
$controller = "UnittypesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeUnitType');
Route::get('/list/{id}',$controller.'listUnitTypes');
Route::delete('/delete/{unittype}',$controller.'destroyUnitType');