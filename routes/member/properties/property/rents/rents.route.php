<?php
$controller = "RentsController@";
Route::get('/{property_id}',$controller.'index');
Route::get('view/{id}',$controller.'viewRent');
Route::post('generate',$controller.'generateRent');
Route::get('/list/{id}',$controller.'listRent');
Route::get('/period/{id}',$controller.'viewRentPeriod');
Route::get('/periods/list/{id}',$controller.'listRentPeriods');
Route::get('/period/rents/list/{id}',$controller.'listPeriodRents');