<?php
$controller = "RentController@";
Route::get('/{id}',$controller.'index');
Route::post('fixed-charges',$controller.'updateFixedCharges');
Route::post('variable-charges',$controller.'updateVariableCharges');
Route::get('/payments/list/{id}',$controller.'listRentPayments');
