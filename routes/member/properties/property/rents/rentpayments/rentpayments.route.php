<?php
$controller = "RentPaymentsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeRentPayment');
Route::post('innitialpayment',$controller.'payInnitialPayment');
Route::post('completepayment',$controller.'completePayment');
Route::get('/list',$controller.'listRentPayments');
Route::delete('/delete/{rentpayment}',$controller.'destroyRentPayment');