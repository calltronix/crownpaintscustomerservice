<?php
$controller = "TenantController@";
Route::get('details/{id}',$controller.'index');
Route::post('/approve/{id}',$controller.'approveTenant');
Route::post('/addunit',$controller.'addUnit');
Route::post('/moveout',$controller.'moveOut');
Route::get('/approve/{id}',$controller.'AddUnitApproveTenant');
Route::delete('/delete/{tenant}',$controller.'destroyTenant');