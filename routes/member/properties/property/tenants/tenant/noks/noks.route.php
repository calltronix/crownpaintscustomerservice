<?php
$controller = "NoksController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeNok');
Route::get('/list/{tenant_id}',$controller.'listNoks');
Route::delete('/delete/{nok}',$controller.'destroyNok');