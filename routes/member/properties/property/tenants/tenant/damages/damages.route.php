<?php
$controller = "DamagesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeDamage');
Route::post('addpayment',$controller.'addPayment');
Route::get('/list/{tenant_id}',$controller.'listDamages');
Route::delete('/delete/{damage}',$controller.'destroyDamage');