<?php
$controller = "RentController@";
Route::get('details/{id}',$controller.'rentDetails');
Route::get('/periods/list/{id}',$controller.'listRentPeriods');
Route::get('/details/{id}',$controller.'listDetails');