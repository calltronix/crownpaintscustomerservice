<?php
$controller = "IssuesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeIssue');
Route::get('/list/{tenant_id}',$controller.'listIssues');
Route::delete('/delete/{issue}',$controller.'destroyIssue');