<?php
$controller = "DepositsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeDeposit');
Route::get('/list/{tenant_id}',$controller.'listDeposits');
Route::delete('/delete/{deposit}',$controller.'destroyDeposit');