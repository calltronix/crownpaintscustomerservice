<?php
$controller = "DamagesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeDamage');
Route::get('/list',$controller.'listDamages');
Route::delete('/delete/{damage}',$controller.'destroyDamage');