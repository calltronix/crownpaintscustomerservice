<?php
$controller = "TenantsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeTenant');
Route::post('uploadtenants',$controller.'uploadTenants');
Route::get('/list/{id}',$controller.'listTenants');
Route::get('tenant-upload/{tenant_upload_id}',$controller.'tenantUpload');
Route::post('tenants-upload/{property_id}',$controller.'tenantsUpload');
Route::delete('/delete/{tenant}',$controller.'destroyTenant');

Route::get('/list/active/{id}',$controller.'listActiveTenants');
Route::get('/list/moved_out/{id}',$controller.'listMovedOutTenants');
Route::get('/list/pending/{id}',$controller.'listPendingTenants');