<?php
$controller = "UnitGroupController@";
Route::get('/{id}',$controller.'index');
Route::get('/list/{property_id}/{id}',$controller.'listUnitGroupUnits');
Route::get('{id}/get-data',$controller.'getData');