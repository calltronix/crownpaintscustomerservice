<?php
$controller = "UnitgroupsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeUnitGroup');
Route::get('/list/{id}',$controller.'listUnitGroups');
Route::get('/list/units/{id}',$controller.'listUnits');
Route::delete('/delete/{unitgroup}',$controller.'destroyUnitGroup');