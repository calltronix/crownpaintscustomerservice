<?php




$controller = "DepartmentsController@";
Route::get('/',$controller.'index');
Route::get('/permissions/{id}',$controller.'getPermissions');
Route::post('/permissions/{id}',$controller.'updatePermissions');
Route::post('/user',$controller.'addUser');
Route::post('/user/remove/{id}',$controller.'removeUser');
Route::post('/',$controller.'storeDepartment');
Route::get('/list',$controller.'listDepartments');
Route::get('/list/users',$controller.'listUsers');