<?php
$controller = "CrownDepartmentsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeCrownDepartment');
Route::get('/list',$controller.'listCrownDepartments');
Route::delete('/delete/{}',$controller.'destroyCrownDepartment');
