<?php
$controller = "DispositionsController@";
Route::get('/{issue_category_id}',$controller.'index');
Route::post('/',$controller.'storeDisposition');
Route::get('/list/{issue_category_id}',$controller.'listDispositions');
Route::delete('/delete/{disposition}',$controller.'destroyDisposition');
