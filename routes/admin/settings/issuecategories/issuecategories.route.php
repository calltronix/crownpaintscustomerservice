<?php
$controller = "IssueCategoriesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeIssueCategory');
Route::get('/list',$controller.'listIssueCategories');
Route::delete('/delete/{issuecategory}',$controller.'destroyIssueCategory');
