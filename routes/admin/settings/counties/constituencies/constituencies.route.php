<?php
$controller = "ConstituencyController@";
Route::get('/{county_id}',$controller.'index');
Route::post('/',$controller.'storeConstituency');
Route::get('/list/{county_id}',$controller.'listConstituencies');
Route::delete('/delete/{constituency}',$controller.'destroyConstituency');
