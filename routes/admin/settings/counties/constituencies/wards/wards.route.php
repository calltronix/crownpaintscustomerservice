<?php
$controller = "WardsController@";
Route::get('/{constituency_id}',$controller.'index');
Route::post('/',$controller.'storeWard');
Route::get('/list/{constituency_id}',$controller.'listWards');
Route::delete('/delete/{ward}',$controller.'destroyWard');
