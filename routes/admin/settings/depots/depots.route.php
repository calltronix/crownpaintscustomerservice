<?php
$controller = "DepotsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeDepot');
Route::get('/list',$controller.'listDepots');
Route::delete('/delete/{depot}',$controller.'destroyDepot');