<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class CrownDepartment extends Model
{
    
	protected $fillable = ["name"];

}
