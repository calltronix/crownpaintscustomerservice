<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    
	protected $fillable = ["name","description","company_id", "permissions", "user_id"];

	public function users(){
	    return $this->hasMany(User::class);
    }

}
