<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Depot extends Model
{
    
	protected $fillable = ["name"];

}
