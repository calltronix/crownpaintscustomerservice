<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Constituency extends Model
{
    
	protected $fillable = ["county_id","name"];

}
