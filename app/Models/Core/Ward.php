<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    
	protected $fillable = ["constituency_id","name"];

}
