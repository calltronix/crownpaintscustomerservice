<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class IssueCategory extends Model
{
    
	protected $fillable = ["name"];

}
