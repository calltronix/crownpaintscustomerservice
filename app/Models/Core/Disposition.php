<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Disposition extends Model
{

	protected $fillable = ["crown_department_id","issue_category_id","name"];

}
