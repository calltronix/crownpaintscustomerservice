<?php

namespace App\Http\Controllers\Admin\Departments;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\Department;
use App\Repositories\SearchRepo;

class DepartmentsController extends Controller
{


        public function index(){

        return view($this->folder.'index',[

        ]);
    }


    public function storeDepartment(){
        request()->validate([
            'name'=> 'required',
            'description'=>'required'
        ]);
        $data = \request()->all();
        $data['user_id'] = request()->user()->id;
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    public function getPermissions($id){
        $department = Department::findOrFail($id);
        $permissions = json_decode(file_get_contents(storage_path("app/system/roles.json")));
        $existing = json_decode($department->permissions);
        return view($this->folder.'permissions',[
           'department'=>$department,
            'permissions'=>$permissions,
            'existing'=>$existing
        ]);
    }

    public function updatePermissions($id){
        $department = Department::findOrFail($id);
        $permissions = \request('permissions');
        if($permissions)
            $permissions = json_encode($permissions);
        $department->permissions = $permissions;
        $department->update();
        return redirect("admin/departments");
    }

    public function addUser(){
        $department = Department::findOrFail(\request('department_id'));
        $user = User::findOrFail(\request('user_id'));
        $user->department_id = $department->id;
        $user->save();
        return redirect()->back();
    }

    public function listDepartments(){
        $departments = Department::where([
            ['id','>',0]

        ]);
        if(\request('all'))
            return $departments->get();
        return SearchRepo::of($departments)
            ->addColumn('system_users',function($department){
                $str='<ul>';
                $users = $department->users;
                foreach ($users as $user){
                    $str.= '<li>'.$user->name.' <span onclick="runPlainRequest(\''.url("admin/departments/user/remove/$user->id").'\')" style="color: #f44336; cursor: pointer"><i class="fa fa-times"></i></span></li>';
                }
                $str.='</ul>';
                $str.='<a onclick="setDepartmentId('.$department->id.')" href="#add_user_modal" data-toggle="modal" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Add</a>';
                return $str;
            })
            ->addColumn('permissions',function($department){
                $str='<ul>';
                $permissions = json_decode($department->permissions);
                if(!$permissions)
                    $permissions=[];
                foreach ($permissions as $permission){
                    $str.= '<li>'.ucwords(str_replace('_',' ',$permission)).'</li>';
                }
                $str.='</ul>';
                $str.='<a onclick="getDepartmentPermission('.$department->id.');" href="#permissions_modal" data-toggle="modal" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                return $str;
            })
            ->addColumn('action',function($department){
                $str = '';
                $json = json_encode($department);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'department_modal\');" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>';
                return $str;
            })

            ->make();
    }
    public function listUsers(){
            $users = User::where([
                ['id','<>', \request()->user()->id],
                ['role', 'admin'],
                ['department_id', 0]
            ])->get();
            return $users;
    }
    public function removeUser($id){
        $user = User::findOrFail($id);

        if($user->id==\request()->user()->id){
            return redirect()->back()->with('notice', ['type' => 'warning', 'message' => 'You cannot remove yourself from a department']);
        }else{
            $user->department_id =0;
            $user->save();
            return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Staff removed successfully']);
        }

    }
}
