<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * returns settings view
     */

    public function index(){
        return view($this->folder. 'depots.index', []);
    }
}
