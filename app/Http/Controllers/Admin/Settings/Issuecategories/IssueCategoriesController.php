<?php

namespace App\Http\Controllers\Admin\Settings\Issuecategories;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Core\IssueCategory;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class IssueCategoriesController extends Controller
{
            /**
         * return issuecategory's index view
         */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }

    /**
     * store issuecategory
     */
    public function storeIssueCategory(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('issuecategories', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return issuecategory values
     */
    public function listIssueCategories(){
        $issuecategories = IssueCategory::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $issuecategories->get();
        return SearchRepo::of($issuecategories)
            ->addColumn('action',function($issuecategory){
                $str = '';
                $json = json_encode($issuecategory);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'issuecategory_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                $str.='&nbsp;&nbsp;<a href="'.url('admin/settings/issuecategories/dispositions/'.$issuecategory->id).'"  class="btn badge btn-secondary btn-sm load-page"><i class="fas fa-cogs"></i> Add Dispositions</a>';

                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/issuecategories/delete').'\',\''.$issuecategory->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete issuecategory
     */
    public function destroyIssueCategory($issuecategory_id)
    {
        $issuecategory = IssueCategory::findOrFail($issuecategory_id);
        $issuecategory->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'IssueCategory deleted successfully']);
    }
}
