<?php

namespace App\Http\Controllers\Admin\Settings\IssueCategories\Dispositions;

use App\Http\Controllers\Controller;
use App\Models\Core\CrownDepartment;
use App\Models\Core\IssueCategory;
use Illuminate\Http\Request;

use App\Models\Core\Disposition;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class DispositionsController extends Controller
{
    /**
     * return disposition's index view
     */
    public function index($issuecategory_id)
    {
        $issuecategory = IssueCategory::find($issuecategory_id);

        return view($this->folder . 'index', [
            'issuecategory_id' => $issuecategory_id,
            'issuecategory'=>$issuecategory->name
        ]);
    }

    /**
     * store disposition
     */
    public function storeDisposition()
    {
        request()->validate([
            'issue_category_id'=>'required|numeric',
            'crown_department_id'=>'nullable|numeric',
            'name'=>'required'

        ]);
        $data = \request()->all();
        if (!isset($data['user_id'])) {
            if (Schema::hasColumn('dispositions', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return disposition values
     */
    public function listDispositions($issue_category_id)
    {
        $dispositions = Disposition::where([
            ['id', '>', 0],
            ['issue_category_id', '=', $issue_category_id]
        ]);
        if (\request('all'))
            return $dispositions->get();
        return SearchRepo::of($dispositions)
            ->addColumn('department', function ($disposition){
                $department = CrownDepartment::find($disposition->crown_department_id);
                if($department)
                    return $department->name;
                return "NONE";
            })
            ->addColumn('issue_category', function ($disposition){
                $issue_category = IssueCategory::find($disposition->issue_category_id);
                if($issue_category)
                    return $issue_category->name;
                return "NONE";
            })
            ->addColumn('disposition', function ($disposition){
               return  $disposition->name;
            })
            ->addColumn('action', function ($disposition) {
                $str = '';
                $json = json_encode($disposition);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'disposition_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/dispositions/delete').'\',\''.$disposition->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete disposition
     */
    public function destroyDisposition($disposition_id)
    {
        $disposition = Disposition::findOrFail($disposition_id);
        $disposition->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Disposition deleted successfully']);
    }
}
