<?php

namespace App\Http\Controllers\Admin\Settings\Depots;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Core\Depot;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class DepotsController extends Controller
{
            /**
         * return depot's index view
         */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }

    /**
     * store depot
     */
    public function storeDepot(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('depots', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return depot values
     */
    public function listDepots(){
        $depots = Depot::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $depots->get();
        return SearchRepo::of($depots)
            ->addColumn('action',function($depot){
                $str = '';
                $json = json_encode($depot);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'depot_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
            //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/depots/delete').'\',\''.$depot->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete depot
     */
    public function destroyDepot($depot_id)
    {
        $depot = Depot::findOrFail($depot_id);
        $depot->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Depot deleted successfully']);
    }
}
