<?php

namespace App\Http\Controllers\Admin\Settings\Counties\Constituencies;

use App\Http\Controllers\Controller;
use App\Models\Core\County;
use Illuminate\Http\Request;

use App\Models\Core\Constituency;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class ConstituencyController extends Controller
{
            /**
         * return constituency's index view
         */
    public function index($county_id){

        return view($this->folder.'index',[
        'county_id'=>$county_id
        ]);
    }

    /**
     * store constituency
     */
    public function storeConstituency(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('constituencies', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return constituency values
     */
    public function listConstituencies($county_id){

        $constituencies = Constituency::where([
            ['id','>',0],
            ['county_id', '=', $county_id]
        ]);

        if(\request('all'))
            return $constituencies->get();
        return SearchRepo::of($constituencies)
            ->addColumn('county', function ($constituency){
                $county = County::find($constituency->county_id);
               return  $county->name;
            })
            ->addColumn('action',function($constituency){
                $str = '';
                $json = json_encode($constituency);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'constituency_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                $str.='&nbsp;&nbsp;<a href="'.url('admin/settings/counties/constituencies/wards/'.$constituency->id).'"  class="btn badge btn-secondary btn-sm load-page"><i class="fas fa-cogs"></i> Manage Wards</a>';

                return $str;
            })->make();
    }

    /**
     * delete constituency
     */
    public function destroyConstituency($constituency_id)
    {
        $constituency = Constituency::findOrFail($constituency_id);
        $constituency->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Constituency deleted successfully']);
    }
}
