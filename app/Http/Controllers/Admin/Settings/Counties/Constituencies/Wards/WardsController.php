<?php

namespace App\Http\Controllers\Admin\Settings\Counties\Constituencies\Wards;

use App\Http\Controllers\Controller;
use App\Models\Core\Constituency;
use Illuminate\Http\Request;

use App\Models\Core\Ward;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class WardsController extends Controller
{
    /**
     * return ward's index view
     */
    public function index($constituency_id)
    {
        $constituency= Constituency::find($constituency_id);
        return view($this->folder . 'index', [
            'constituency_id' => $constituency_id,
            'constituency' => $constituency->name,
        ]);
    }

    /**
     * store ward
     */
    public function storeWard()
    {
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if (!isset($data['user_id'])) {
            if (Schema::hasColumn('wards', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return ward values
     */
    public function listWards($constituency_id)
    {
        $wards = Ward::where([
            ['id', '>', 0],
            ['constituency_id', '=', $constituency_id]
        ]);
        if (\request('all'))
            return $wards->get();
        return SearchRepo::of($wards)
            ->addColumn('constituency', function ($ward){
                $constituency= Constituency::find($ward->constituency_id);
                return $constituency->name;
            })
            ->addColumn('action', function ($ward) {
                $str = '';
                $json = json_encode($ward);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'ward_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/wards/delete').'\',\''.$ward->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete ward
     */
    public function destroyWard($ward_id)
    {
        $ward = Ward::findOrFail($ward_id);
        $ward->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Ward deleted successfully']);
    }
}
