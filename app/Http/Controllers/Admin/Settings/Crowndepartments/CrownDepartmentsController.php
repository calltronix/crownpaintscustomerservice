<?php

namespace App\Http\Controllers\Admin\Settings\Crowndepartments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Core\CrownDepartment;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class CrownDepartmentsController extends Controller
{
            /**
         * return crowndepartment's index view
         */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }

    /**
     * store crowndepartment
     */
    public function storeCrownDepartment(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('crowndepartments', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return crowndepartment values
     */
    public function listCrownDepartments(){
        $crowndepartments = CrownDepartment::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $crowndepartments->get();
        return SearchRepo::of($crowndepartments)
            ->addColumn('action',function($crowndepartment){
                $str = '';
                $json = json_encode($crowndepartment);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'crowndepartment_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
            //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/crowndepartments/delete').'\',\''.$crowndepartment->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete crowndepartment
     */
    public function destroyCrownDepartment($crowndepartment_id)
    {
        $crowndepartment = CrownDepartment::findOrFail($crowndepartment_id);
        $crowndepartment->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'CrownDepartment deleted successfully']);
    }
}
