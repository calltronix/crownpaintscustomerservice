<?php

namespace App\Http\Controllers\Admin\Users;

use App\Models\Core\Assign;
use App\Models\Core\Order;
use App\Notifications\GeneralNotification;
use App\Notifications\SendPasswordNotification;
use App\Repositories\SearchRepo;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{


    public function index()
    {
        return view($this->folder. '.index' , [
        ]);
    }

    public function listUsers()
    {
        $users = User::where('id', '!=', \request()->user()->id);

//

        if (\request('all'))
            return $users->get();
        return SearchRepo::of($users)
            ->addColumn('action', function ($user) {
                $str = '';
                $json = json_encode($user);
                $str .= '<a href="' . url("admin/users/user/$user->id") . '"   class="btn btn-info btn-sm load-page"><i class="fa fa-eye"></i> View</a>';
                return $str;
            })->make();


    }
    public function viewUser($id){
        $user = User::where('id', $id)->first();


        return view($this->folder. 'profile', [
            'user'=>$user
        ]);

    }
    public function userProfile(){
        request()->validate([
            'name'=> 'required',
            'email'=>'required',
            'phone'=>'required',

        ]);
        request()->validate([
            'phone' => ['unique:users,phone,'.\request('user_id'),'regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],

        ]);
        $user=User::find(\request('user_id'));
        $user->name=\request('name');
        $user->email=\request('email');
        $user->phone=\request('phone');
        $user->save();
        return back()->with('notice', ['type' => 'success', 'message' => 'User Profile Updated Successfully']);
    }
    public function updatePassword(){
        request()->validate([
            'password'=>'required|confirmed',
        ]);
        $user=User::find(\request('user_id'));
        $user->password = Hash::make(request('password'));
        $user->update();
        $password=request('password');
        $user->notify(new SendPasswordNotification($password));
        return redirect()->back();
    }
}
