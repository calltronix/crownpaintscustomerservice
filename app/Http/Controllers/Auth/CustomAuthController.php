<?php

namespace App\Http\Controllers\Auth;

use App\Models\Core\Company;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class CustomAuthController extends Controller
{
    /**
     * Login user
     */

    public function login(){
        \request()->validate([
            'email'=>'required|email',
            'password'=>'required'
        ]);
        if(Auth::attempt(['email'=>request('email'),'password'=>\request('password')])){
            $user = Auth::user();
            if (session('url.intended'))
                return ['force_redirect'=>session('url.intended')];
            return ['force_redirect'=>'home'];
        }
        $email = \request('email');

        if(Auth::attempt(['email'=>request('email'),'password'=>\request('password')])){
            //user logged in
            $user = Auth::user();
            return ['force_redirect'=>'home'];

        }
        else{
            return response(['errors'=>['email'=>['Invalid email or password']]],422);
        }
    }

    /**
     * Register New user with
     * master company
     */
    public function registerUser(){
        $this->validateData();

        $user_data = [
            'name'=>request()->name,
            'email'=>request()->email,
            'phone'=>request()->phone,
            'password'=>request()->password,
            'form_model'=>User::class,
        ];
        $user = $this->autoSaveModel($user_data);
        Auth::login($user);

        return ['force_redirect'=>'home'];
    }

    protected function formatPhone($phone){
        $len = strlen($phone);
        if($len==10){
            $phone = "repl".$phone;
            $phone = str_replace('repl07','+2547',$phone);
        }
        if($len==12){
            $phone = '+'.$phone;
        }

        return $phone;
    }

    public function validateData(){
        request()->validate([
            'name' => ['required','string','max:255'],
            'email' => ['required','string','email','max:255','unique:users'],//
            'password' => ['required','string','min:6','confirmed'],
            'phone' => ['unique:users,phone','regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],

        ]);
    }
}
