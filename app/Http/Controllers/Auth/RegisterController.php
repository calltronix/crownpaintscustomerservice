<?php

namespace App\Http\Controllers\Auth;

use App\Models\Core\Department;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;


    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['unique:users,phone','regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    public function register(){

        $this->validateData();

        $user_data = [
            'name'=>request()->name,
            'email'=>request()->email,
            'phone'=>request()->phone,
            'password'=>request()->password,
            'role'=>'admin',
            'form_model'=>User::class,
            'email_verified_at'=>date("Y-m-d H:i:s")
        ];
        $user = $this->autoSaveModel($user_data);
        Auth::login($user);
        event(new Registered($user));
        $permissions = json_decode(file_get_contents(storage_path("app/system/roles.json")));
        $slugs = [];
        foreach ($permissions->admin as $admin){
            $slugs[]= $admin->slug;
        }
        $roles =json_encode($slugs);
        $department = new Department();
        $department->name ='Admins';
        $department->description ='System Administrators';
        $department->permissions =$roles;
        $department->user_id =$user->id;
        $department->save();
        $user->department_id = $department->id;
//        $user->save();
        return ['force_redirect'=>'admin'];
    }

    protected function formatPhone($phone){
        $len = strlen($phone);
        if($len==10){
            $phone = "repl".$phone;
            $phone = str_replace('repl07','+2547',$phone);
        }
        if($len==12){
            $phone = '+'.$phone;
        }

        return $phone;
    }

    public function validateData(){
        request()->validate([
            'name' => ['required','string','max:255'],
            'email' => ['required','string','email','max:255','unique:users'],//
            'password' => ['required','string','min:6','confirmed'],
            'phone' => ['unique:users,phone','regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],


        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

//    protected function create(array $data)
//    {
//        return User::create([
//            'name' => $data['name'],
//            'email' => $data['email'],
//            'password' => Hash::make($data['password']),
//        ]);
//    }

}
