<?php

namespace App\Http\Controllers\User;

use App\Repositories\SearchRepo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Session;

use Notification;
use App\Notifications\GeneralNotification;

class UserController extends Controller
{
    public function profile()
    {

        $user = request()->user();

        return view($this->folder . 'profile', [
            'user' => $user
        ]);
    }

    public function updateProfile($user_id)
    {
        request()->validate([
            'name' => 'required',
//            'username' => 'required','unique:users,username,'.auth()->id(),
            'phone' => ['unique:users,phone,'.auth()->id(),'regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],
        ]);
        request()->user()->update([
            'name' => request()->name,
            'phone' => request()->phone
        ]);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Profile updated successfully']);
    }


    public function updateProfilePicture()
    {
        request()->validate([
            'avatar' => 'mimes:jpeg,jpg,png|required|max:2048'
        ]);
        $user = request()->user();

        $image_path = storage_path() . '/app/public/profile-pics/' . $user->avatar;
        if ($user->avatar != "user.jpg" && @getimagesize($image_path))
            unlink($image_path);

        $image = request()->file('avatar');
        $ext = $image->getClientOriginalExtension();
        $image_original_name = $image->getClientOriginalName();
        $image_name = str_replace('.' . $ext, '', $image_original_name);
        $name = $user->id . '_' . $image_name . "." . $ext;
        $user->avatar = $name;
        $user->update();
        $image->move(storage_path() . '/app/public/profile-pics', $name);
        return redirect()->back();
    }

    public function updatePassword()
    {
        $currentuser = request()->user();

        request()->validate([
//            'old_password' => ['required', new OldPasswordValidator()],
            'password' => 'required'
        ]);
        $currentuser->password = bcrypt(request('password'));
        $currentuser->update();

        $user = request()->user();
//        Notification::send($user, new GeneralNotification('password_update',$currentuser));
        return redirect()->back();
    }

    /**
     * set my default branch
     */
    public function setMyDefaultBranch()
    {
        request()->validate([
            'branch_id' => 'required|numeric'
        ]);
        $cart = $this->cartName();
        request()->user()->update([
            'default_branch' => request()->branch_id
        ]);
        Session::forget($cart);
        return redirect()->back()->with(['notice', ['type' => 'success', 'message' => 'Default set successfully']]);
    }
}
