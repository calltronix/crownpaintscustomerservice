<?php
/**
 * Created by PhpStorm.
 * User: iankibet
 * Date: 5/12/18
 * Time: 10:16 AM
 */

namespace App\Repositories;


class StatusRepository
{
    public static function getCompanyStatus($state){
        $statuses = [
            'inactive'=>0,
            'active'=>1,
            'suspended'=>2
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }
            return $states;
        }
        return $statuses[$state];
    }
    public static function getPropertyStatus($state){
        $statuses = [
            'inactive'=>0,
            'active'=>1,
            'suspended'=>2
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }
            return $states;
        }
        return $statuses[$state];
    }

    public static function getUserStatus($state){
        $statuses = [
            'inactive'=>0,
            'active'=>1,
            'suspended'=>2,
            'deactivated'=>3
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }

            return $states;
        }

        return $statuses[$state];
    }

    /**
     * get unit status ie.
     *  room is vacant, occupied or in maintenance
     */
    public static function getUnitStatus($state){
        $statuses = [
            'vacant'=>0,
            'occupied'=>1,
            'maintenance'=>2
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }

            return $states;
        }

        return $statuses[$state];
    }

    /**
     * get unit tenant status ie.
     *  room occupied or vacated
     */
    public static function getUnitTenantStatus($state){
        $statuses = [
            'vacated'=>0,
            'occupied'=>1
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }

            return $states;
        }

        return $statuses[$state];
    }

    public static function getMonthlyChargeStatus($state){
        $statuses = [
            'inactive'=>0,
            'active'=>1
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }

            return $states;
        }

        return $statuses[$state];
    }
    public static function getRecurringExpensesStatus($state){
        $statuses = [
            'inactive'=>0,
            'active'=>1
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }

            return $states;
        }

        return $statuses[$state];
    }
    public static function getExpenseConfig($state){
        $statuses = [
            'inactive'=>0,
            'active'=>1
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }

            return $states;
        }

        return $statuses[$state];
    }

    /**
     * get rent status
     */
    public static function getRentStatus($state){
        $statuses = [
            'not_paid'=>0,
            'partially_paid'=>1,
            'paid'=>2,
            'defaulted'=>3
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }
            return $states;
        }
        return $statuses[$state];
    }

    public static function getInvoiceStatus($state){
        $statuses = [
            'pending'=>0,
            'send'=>1
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }
            return $states;
        }
        return $statuses[$state];
    }


    public static function getQuotationStatus($state){
        $statuses = [
            'pending'=>0,
            'send'=>1,
            'invoice_generated'=>2,
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }
            return $states;
        }
        return $statuses[$state];
    }


    public static function getTenantStatus($state){
        $statuses = [
            'pending'=>0,
            'active'=>1,
            'moved_out'=>2,
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }
            return $states;
        }
        return $statuses[$state];
    }


    public static function getIssueStatus($state){
        $statuses = [
            'pending'=>0,
            'resolved'=>1,
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }
            return $states;
        }
        return $statuses[$state];
    }

    public static function getDepostRefundStatus($state){
        $statuses = [
            'pending'=>0,
            'refunded'=>1,
        ];
        if(is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }
            return $states;
        }
        return $statuses[$state];
    }
}