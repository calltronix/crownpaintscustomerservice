<?php

namespace App;

use App\Models\Core\Company;
use App\Models\Core\Department;
use App\Models\Core\UnitGroup;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','company_id','property_id','phone','role','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function department(){
        return $this->belongsTo(Department::class);
    }

    public function isAllowedTo($slug){
        $department = $this->department;

        if(!$department)
            return false;
        $permissions = json_decode($department->permissions);
        if(in_array($slug,$permissions))
            return true;
        return false;
    }


}
