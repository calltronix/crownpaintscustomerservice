<?php
/**
 * Created by PhpStorm.
 * User: kemboi
 * Date: 3/1/19
 * Time: 2:30 PM
 */
?>
@extends('layouts.admin')
@section('title')
    My Profile
@endsection

@section('bread_crumb')
    {{--     <li class="breadcrumb-item"><a class="load-page" href="{{ url('admin/users') }}">Users</a></li>--}}
    <li class="breadcrumb-item active">@yield('title')</li>
@endsection
@section('content')

    <div class="col-sm-12">
        <h6 class="card-title float-left">Background Info</h6>
        <a href="#details_modal_2" data-toggle="modal" class="btn btn-sm btn-outline-success float-right"><i class="fa fa-edit"></i> Edit</a>

    </div>

    <div class="row col-sm-12 justify-content-center">


        <div class="table-responsive col-sm-10">

            <table class="table table-striped table-condensed table-hover">
                <tbody>
                <tr>
                    <th> Name</th>
                    <td>{{ $user->name }}</td>
                </tr>

                <tr>
                    <th>Phone Number</th>
                    <td>{{ $user->phone }}</td>
                </tr>
                <tr>
                    <th>Role</th>
                    <td>{{ $user->role }}</td>
                </tr>

                <tr>
                    <th>Email</th>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <th>Password</th>
                    <td>********
                        <a href="#update_password" data-toggle="modal" class="btn btn-sm btn-outline-success float-right "><i class="fa fa-edit"></i>Edit</a>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>

    </div>

    @include('common.auto_modal',[
        'modal_id'=>'details_modal_2',
        'modal_title'=>'Personal Details',
        'modal_content'=>Form::autoForm(['name','phone'],'user/profile',null,Auth::user()),
    ])
    @include('common.auto_modal',[
        'modal_id'=>'update_password',
        'modal_title'=>'New Password',
        'modal_content'=>Form::autoForm(['password','password_confirmation'],'user/password'),
    ])
    {{--@include('common.auto_modal',[--}}
    {{--'modal_id'=>'update_profile_picture',--}}
    {{--'modal_title'=>'Profile Picture',--}}
    {{--'modal_content'=>Form::autoForm(['avatar'],'user/profile-picture'),--}}
    {{--])--}}
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $(".tooltip").tooltip("hide");
    </script>

@endsection
