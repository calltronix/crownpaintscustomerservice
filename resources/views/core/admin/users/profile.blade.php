@extends('layouts.admin')

@section('title')
    System User
@endsection



@section('page_title')
    <span class="font-weight-semibold">Home</span> -
   User Profile
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    @include('common.model_display',[
                        'keys'=>['name','email','phone','role','created_at'],
                        'model'=>$user
                    ])
                </div>
                <div class="col-md-4">
                    <h3>Main Actions</h3>
                    <a onclick="editUser()" data-toggle="modal" class="btn btn-outline-info">
                        <img style="max-height: 50px;" src="{{ url(env('CDN_HOST','')."/img/edit-profile.png") }}">
                        <br/>
                        Edit Details
                    </a>
                    <a onclick="updatePassword()" data-toggle="modal" class="btn btn-outline-info">
                        <img style="max-height: 50px;" src="{{ url(env('CDN_HOST','')."/img/edit-password.png") }}">
                        <br/>
                        Change Password
                    </a>

                </div>
            </div>

        </div>

    </div>

    @include('common.auto_modal',[
        'modal_id'=>'edit_profile_details',
        'modal_title'=>'Edit User Profile ',
        'modal_content'=>Form::autoForm(['name','email','phone', 'hidden_user_id'],"admin/users/user/update")
    ])
    @include('common.auto_modal',[
           'modal_id'=>'update_password',
           'modal_title'=>'New Password',
           'modal_content'=>Form::autoForm(['password','password_confirmation','hidden_user_id'],'admin/users/user/update-password'),
       ])



    <script type="text/javascript">
        function editUser() {
            $("input[name='name']").val('{{ $user->name }}');
            $("input[name='email']").val('{{ $user->email }}');
            $("input[name='phone']").val('{{ $user->phone }}');
            $("input[name='user_id']").val('{{ $user->id }}');

            $("#edit_profile_details").modal('show');
        }
        function updatePassword() {
            $("input[name='user_id']").val('{{ $user->id }}');
            $("#update_password").modal('show');
        }


    </script>

@endsection

