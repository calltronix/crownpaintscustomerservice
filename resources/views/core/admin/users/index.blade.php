@extends('layouts.admin')

@section('title')
    System Users
@endsection

@section('bread_crumb')
    {{--     <li class="breadcrumb-item"><a class="load-page" href="{{ url('admin/users') }}">Users</a></li>--}}
    <li class="breadcrumb-item active">@yield('title')</li>
@endsection

@section('content')
    <div class="card">-

        <div class="card-body">
            @include('common.bootstrap_table_ajax',[
             'table_headers'=>["id","name","email","phone","action"],
             'data_url'=>'admin/users/list',
             'base_tbl'=>'users'
             ])
        </div>
    </div>


@endsection
