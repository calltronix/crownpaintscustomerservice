@extends('core.admin.settings.index')
@section('title')
    Manage  Departments
@endsection
@section('settings-content')
<a href="#crowndepartment_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD CROWNDEPARTMENT</a>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","name","action"],
    'data_url'=>'admin/settings/crowndepartments/list',
    'base_tbl'=>'crown_departments'
    ])

    @include('common.auto_modal',[
        'modal_id'=>'crowndepartment_modal',
        'modal_title'=>'CROWNDEPARTMENT FORM',
        'modal_content'=>Form::autoForm(\App\Models\Core\CrownDepartment::class,"admin/settings/crowndepartments")
    ])
@endsection

