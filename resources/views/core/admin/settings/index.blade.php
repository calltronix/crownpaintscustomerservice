@extends('layouts.admin')

@section('title')
    CRM Settings
@endsection
@section('bread_crumb')
    {{--     <li class="breadcrumb-item"><a class="load-page" href="{{ url('admin/users') }}">Users</a></li>--}}
    <li class="breadcrumb-item active">@yield('title')</li>
@endsection
@section('content')
    <style>
        .inbox-leftbar {
            width: 200px;
            float: left;
            padding: 0 10px 10px 10px;
        }
        .inbox-rightbar {
            margin: -1.5rem 0 -1.5rem 200px;
            border-left: 5px solid #f5f6f8;
            padding: 1.5rem 0 1.5rem 25px;
        }
    </style>

    <div class="card-box">
        <!-- Left sidebar -->
        <div class="inbox-leftbar">
            <div class="mail-list mt-1">
                <a href="{{url('admin/settings/depots')}}" class="list-group-item border-0 load-page"><i class=" ti ti-angle-double-right font-18 align-middle mr-1"></i>Depots</a>
                <a href="{{url('admin/settings/crowndepartments')}}" class="list-group-item border-0 load-page"><i class="ti ti-angle-double-right font-18 align-middle mr-1"></i>Department</a>
                <a href="{{url('admin/settings/issuecategories')}}" class="list-group-item border-0 load-page"><i class="ti ti-angle-double-right font-18 align-middle mr-1"></i>Issue Categories</a>
{{--                <a href="#" class="list-group-item border-0"><i class="ti ti-angle-double-right font-18 align-middle mr-1"></i>Dispositions</a>--}}
                <a href="{{url('admin/settings/counties')}}" class="list-group-item border-0 load-page"><i class="ti ti-angle-double-right font-18 align-middle mr-1"></i>Counties</a>
            </div>



        </div>
        <!-- End Left sidebar -->

        <div class="inbox-rightbar">
            @yield('settings-content')
        </div>
        <!-- end inbox-rightbar-->

        <div class="clearfix"></div>
    </div> <!-- end card-box -->
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $(".tooltip").tooltip("hide");
        })

    </script>


@endsection
