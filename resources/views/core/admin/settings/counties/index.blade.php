@extends('core.admin.settings.index')

@section('title') Manage Counties @endsection

@section('settings-content')
<a href="#county_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD COUNTY</a>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","name","action"],
    'data_url'=>'admin/settings/counties/list',
    'base_tbl'=>'counties'
    ])

    @include('common.auto_modal',[
        'modal_id'=>'county_modal',
        'modal_title'=>'COUNTY FORM',
        'modal_content'=>Form::autoForm(\App\Models\Core\County::class,"admin/settings/counties")
    ])
@endsection
