@extends('core.admin.settings.index')

@section('title') Constituencies @endsection
@section('bread_crumb')
    <li class="breadcrumb-item">
        <a class="load-page" href="{{ url('admin/settings/counties') }}">Counties</a></li>
    <li class="breadcrumb-item active">@yield('title')</li>
@endsection
@section('settings-content')
    <a href="{{url('admin/settings/counties')}}" class="btn badge btn-secondary btn-sm load-page"><i
            class=" fe  ti-angle-double-left"></i> Back to Counties</a>
    <a href="#constituency_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
            class="fa fa-plus"></i> ADD CONSTITUENCY</a>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","county","name","action"],
    'data_url'=>'admin/settings/counties/constituencies/list/'.$county_id,
    'base_tbl'=>'constituencies'
    ])

    @include('common.auto_modal',[
        'modal_id'=>'constituency_modal',
        'modal_title'=>'CONSTITUENCY FORM',
        'modal_content'=>Form::autoForm(['hidden_county_id','name', 'form_model'=>\App\Models\Core\Constituency::class ],"admin/settings/counties/constituencies")
    ])
@endsection
<script type="text/javascript">
    $(function () {
        $('input[name="county_id"]').val(`{{$county_id}}`)
    })

</script>

