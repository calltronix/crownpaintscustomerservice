@extends('core.admin.settings.index')

@section('title') Constituencies @endsection
@section('bread_crumb')
    <li class="breadcrumb-item">
        <a class="load-page" href="{{ url('admin/settings/counties') }}">Counties</a></li>
    <li class="breadcrumb-item active">@yield('title')</li>
@endsection

@section('settings-content')
    <a href="{{url()->previous()}}" class="btn badge btn-secondary btn-sm load-page"><i
            class=" fe  ti-angle-double-left"></i> Back to {{$constituency}}</a>
    <a href="#ward_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
            class="fa fa-plus"></i> ADD WARD</a>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","constituency","name","action"],
    'data_url'=>'admin/settings/counties/constituencies/wards/list/'.$constituency_id,
    'base_tbl'=>'wards'
    ])

    @include('common.auto_modal',[
        'modal_id'=>'ward_modal',
        'modal_title'=>'WARD FORM',
       'modal_content'=>Form::autoForm(['hidden_constituency_id','name', 'form_model'=>\App\Models\Core\Ward::class ],"admin/settings/counties/constituencies/wards")
    ])
@endsection

<script type="text/javascript">
    $(function () {
        $('input[name="constituency_id"]').val(`{{$constituency_id}}`)
    })

</script>
