@extends('core.admin.settings.index')

@section('title')  {{$issuecategory}} Dispositions @endsection
@section('bread_crumb')
    <li class="breadcrumb-item">
        <a class="load-page" href="{{ url('admin/settings/counties') }}">{{$issuecategory}} Dispositions</a></li>
    <li class="breadcrumb-item active">@yield('title')</li>
@endsection
@section('settings-content')
    <a href="{{url('admin/settings/issuecategories')}}" class="btn badge btn-secondary btn-sm load-page"><i
            class=" fe  ti-angle-double-left"></i> Back to  Issue Categories</a>
    <a href="#disposition_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
            class="fa fa-plus"></i> {{ strtoupper($issuecategory) }} DISPOSITION</a>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","department","issue_category","disposition","action"],
    'data_url'=>'admin/settings/issuecategories/dispositions/list/'.$issuecategory_id,
    'base_tbl'=>'dispositions'
    ])

    @include('common.auto_modal',[
        'modal_id'=>'disposition_modal',
        'modal_title'=>'DISPOSITION FORM',
        'modal_content'=>Form::autoForm(['hidden_issue_category_id','crown_department_id','name', 'form_model'=>\App\Models\Core\Disposition::class ],"admin/settings/issuecategories/dispositions")

    ])

    <script type="text/javascript">
        $(".label_crown_department_id").html("Department");
        $("input[name='issue_category_id']").val('{{$issuecategory_id}}');

        $(document).ready(function(){

            autoFillSelect('crown_department_id', '{{ url('admin/settings/crowndepartments/list?all=1') }}')
        });
    </script>
@endsection


