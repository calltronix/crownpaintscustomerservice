@extends('core.admin.settings.index')
@section('title')
    Manage Depots
@endsection
@section('settings-content')
    <a href="#depot_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD DEPOT</a>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","name","action"],
    'data_url'=>'admin/settings/depots/list',
    'base_tbl'=>'depots'
    ])

    @include('common.auto_modal',[
        'modal_id'=>'depot_modal',
        'modal_title'=>'DEPOT FORM',
        'modal_content'=>Form::autoForm(\App\Models\Core\Depot::class,"admin/settings/depots")
    ])
@endsection

