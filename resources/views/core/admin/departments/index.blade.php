@extends('layouts.admin')

@section('title')
    Departments
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <a href="#department_modal" class="btn btn-primary" data-toggle="modal"><i class="fa fa-plus"></i> ADD DEPARTMENT</a>
            <hr/>
            @include('common.bootstrap_table_ajax',[
            'table_headers'=>["id","name","description","permissions","system_users","action"],
            'data_url'=>'admin/departments/list',
            'base_tbl'=>'departments'
            ])
            @include('common.auto_modal',[
                'modal_id'=>'department_modal',
                'modal_title'=>'DEPARTMENT FORM',
                'modal_content'=>Form::autoForm(['name','description', 'form_model'=>\App\Models\Core\Department::class],"admin/departments")
            ])
            @include('common.auto_modal',[
                'modal_id'=>'permissions_modal',
                'modal_title'=>'Edit Department Permissions',
                'modal_content'=>'<div class="permissions_section"></div>'
            ])
            @include('common.auto_modal',[
                'modal_id'=>'add_user_modal',
                'modal_title'=>'Add User to Group',
                'modal_content'=>Form::autoForm(['hidden_department_id','user_id'],"admin/departments/user")
            ])
            <script type="text/javascript">
                function getDepartmentPermission(id){
                    ajaxLoad('{{ url("admin/departments/permissions") }}/'+id,'permissions_section');
                }

                function setDepartmentId(id){
                    $("input[name='department_id']").val(id);
                }
                $(document).ready(function(){
                    autoFillSelect('user_id','{{ url("admin/departments/list/users") }}');
                });
            </script>
        </div>
    </div>

@endsection
