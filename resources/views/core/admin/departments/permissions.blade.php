<form class="form-horizontal" method="post" action="{{ request()->url() }}">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="control-label">Permissions</label>
        <div>
            @foreach($permissions->admin as $permission)
                <input value="{{ $permission->slug }}" {{ @in_array($permission->slug,$existing) ? 'checked':'' }} type="checkbox" name="permissions[]">&nbsp;{{ ucwords(str_replace('_',' ',$permission->slug)) }}<br/>
            @endforeach
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success">Update</button>
    </div>
</form>