@if(Request::ajax())
    @if(isset($_GET['t_optimized']))
        @yield('t_optimized')
    @elseif(isset($_GET['ta_optimized']))
        @yield('ta_optimized')
    @else
        <div class="system-title"></div>
        @yield('content')
    @endif
    @include('common.essential_js')
@else
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ config('app.name', 'CrownPaints Customer Service') }}</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
              type="text/css">
        <link rel="shortcut icon" href="{{url('/')}}/img/crownicon.png">

        <!-- App css -->
        <link href="{{ url('backend') }}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{ url('backend') }}/assets/css/icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{ url('backend') }}/assets/css/app.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{ url('css/fontawesome-all.css') }}" rel="stylesheet">
        <!-- /global stylesheets -->


    </head>

    <body>

    <div class="account-pages mt-5 mb-5">
        <div class="container">

            <div class="row justify-content-center system-container">


            @yield('content')


            <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end page -->


    <footer class="footer footer-alt">
        {{date('Y')}} &copy; Crown Paints PLC created by <a href="" target="_blank" class="text-primary">Calltronix Kenya
            Limited</a>
    </footer>
    <!-- /page content -->
    <!-- Vendor js -->
    <script src="{{ url('backend') }}/assets/js/vendor.min.js"></script>

    <!-- App js -->
    <script src="{{ url('backend') }}/assets/js/app.min.js"></script>

    {{--<script src="{{ url(env('CDN_HOST').'js/fontawesome.min.js') }}"></script>--}}
    <script src="{{ asset('js/jquery.history.js') }}"></script>
    <script src="{{ url('js/jquery.datetimepicker.js') }}"></script>
    <script src="{{ url('js/jquery.form.js') }}"></script>
    @include('common.javascript')
    @stack('footer-scripts')
    <!-- Custom main Js -->
    <input type="hidden" name="material_page_loaded" value="1">
    <script src="{{ url('sweetalert/dist/sweetalert.min.js') }}"></script>
    </body>
    </html>
@endif

