@extends('auth.main')



@section('content')
    <div class="col-md-8 col-lg-6 col-xl-6">
        <div class="card">

            <div class="card-body p-4">

                <div class="text-center w-75 m-auto">
                    <a href="{{url('/')}}">
                        <span><img src="{{ url('/') }}/img/Crownpaints-PLC.png" alt=""></span>
                    </a>
                    <p class="text-muted mb-4 mt-3">Enter your email address and password to access customer service portal.</p>
                </div>

                <form class="ajax-post" method="POST" action="{{ url('login') }}">
                    @csrf
                    <div class="form-group mb-3">
                        <label for="emailaddress">Email address</label>
                        <input class="form-control" type="email" name="email" id="email"  placeholder="Enter your email">
                    </div>

                    <div class="form-group mb-3">
                        <label for="password">Password</label>
                        <input class="form-control" name="password" type="password"  id="password" placeholder="Enter your password">
                    </div>

                    <div class="form-group mb-3">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="remember" id="checkbox-signin" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                        </div>
                    </div>

                    <div class="form-group mb-0 text-center">
                        <button class="submit-btn btn btn-primary btn-block" type="submit"> Log In </button>
                    </div>

                </form>


            </div> <!-- end card-body -->
        </div>
        <!-- end card -->

        <div class="row mt-3">
            <div class="col-12 text-center">
                <p> <a href="{{ route('password.request') }}" class="text-muted ml-1 load-page">Forgot your password?</a></p>
                <p class="text-muted">Don't have an account? <a href="{{ url('register') }}" class="text-primary font-weight-medium ml-1 load-page">Sign Up</a></p>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div>
@endsection
