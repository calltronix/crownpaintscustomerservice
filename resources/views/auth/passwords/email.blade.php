@extends('auth.main')

@section('content')
    <div class="col-md-8 col-lg-6 col-xl-5">

        <div class="card">

            <div class="card-body p-4">

                <div class="text-center w-75 m-auto">
                    <a href="{{url('/')}}">
                        <span><img src="{{ url('/') }}/img/Crownpaints-PLC.png" alt=""></span>
                    </a>
                    <p class="text-muted mb-4 mt-3">Enter your email address and we'll send you an email with instructions to reset your password.</p>
                </div>

                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-group mb-3">
                        <label for="emailaddress">Email address</label>
                        <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" type="email" id="email" required="" placeholder="Enter your email">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group mb-0 text-center">
                        <button class="btn btn-primary btn-block" type="submit"> Reset Password </button>
                    </div>

                </form>

            </div> <!-- end card-body -->
        </div>
        <!-- end card -->

        <div class="row mt-3">
            <div class="col-12 text-center">
                <p class="text-muted">Back to <a href="{{url('login')}}" class="text-primary font-weight-medium ml-1 load-page">Log in</a></p>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- end col -->

@endsection
