@extends('auth.main')

@section('content')

    <div class="col-md-8 col-lg-6 col-xl-6">
        <div class="card">

            <div class="card-body p-4">

                <div class="text-center w-75 m-auto">
                    <a href="{{url('/')}}">
                        <span><img src="{{ url('/') }}/img/Crownpaints-PLC.png" alt=""></span>
                    </a>
                    <p class="text-muted mb-4 mt-3">You can now reset your password using your email address below.</p>
                </div>

                <form  method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group mb-3">
                        <label for="emailaddress">Email address</label>
                        <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"  value="{{ $email ?? old('email') }}" type="email" name="email" id="email"  placeholder="Enter your email" required>
                    </div>

                    <div class="form-group mb-3">
                        <label for="password">Password</label>
                        <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required  type="password"  id="password" placeholder="Enter your password">
                    </div>
                    <div class="form-group mb-3">
                        <label for="password_confirmation">Confirm Password</label>
                        <input class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required  type="password"  id="password_confirmation" placeholder="Enter your password again">
                    </div>



                    <div class="form-group mb-0 text-center">
                        <button class="submit-btn btn btn-primary btn-block" type="submit"> Reset Password </button>
                    </div>

                </form>


            </div> <!-- end card-body -->
        </div>
        <!-- end card -->


        <!-- end row -->

    </div>
@endsection
