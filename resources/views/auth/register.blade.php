@extends('auth.main')
@section('title')
    Register
@endsection
@section('content')
    <!-- Registration form -->
    <div class="col-md-8 col-lg-6 col-xl-6">
        <div class="card">

            <div class="card-body p-4">

                <div class="text-center w-75 m-auto">
                    <a href="index.html">
                        <span><img src="{{ url('/') }}/img/Crownpaints-PLC.png" alt=""></span>
                    </a>
                    <p class="text-muted mb-4 mt-3">Don't have an account? Create your own account, it takes less than a
                        minute</p>
                </div>

                <form method="POST" action="{{ url('register') }}" class="ajax-post">
                    @csrf
                    <div class="form-group">
                        <label for="fullname">Full Name</label>
                        <input class="form-control" name="name" type="text" id="name" placeholder="Enter your name"
                               >
                    </div>
                    <div class="form-group">
                        <label for="emailaddress">Email address</label>
                        <input class="form-control" type="email" id="email" name="email"
                               placeholder="Enter your email">
                    </div>
                    <div class="form-group">
                        <label for="emailaddress">Phone</label>
                        <input class="form-control" type="text" id="phone" name="phone"
                               placeholder="Enter your phone">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" name="password" id="password"
                               placeholder="Enter your password">
                    </div>
                    <div class="form-group">
                        <label for="password">Confirm Password</label>
                        <input class="form-control" type="password" name="password_confirmation" id="password"
                               placeholder="Enter your password">
                    </div>

                    <div class="form-group mb-0 text-center">
                        <button class="submit-btn btn btn-primary btn-block" type="submit"> Sign Up</button>
                    </div>

                </form>


            </div> <!-- end card-body -->
        </div>
        <!-- end card -->

        <div class="row mt-3">
            <div class="col-12 text-center">
                <p class="text-muted">Already have account? <a href="{{ url('login') }}"
                                                               class="text-primary font-weight-medium ml-1 load-page">Sign
                        In</a></p>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div>

@endsection
