@if(Request::ajax())
    @if(isset($_GET['t_optimized']))
        @yield('t_optimized')
    @elseif(isset($_GET['ta_optimized']))
        @yield('ta_optimized')
    @else
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a class="load-page" href="{{ url(Auth::user()->role) }}">Home</a>
                                </li>
                                @yield('bread_crumb')
                                &nbsp;&nbsp;&nbsp;
                                <a data-toggle="tooltip" title="Click to Reload Page"
                                   href="{{ url(\Illuminate\Support\Facades\Request::fullUrl()) }}"
                                   class=" load-page" data-action="reload"><i
                                        class="font-weight-bold remixicon-refresh-line icon-2x"></i>
                                </a>
                            </ol>
                        </div>
                        <h4 class="page-title system-title">@yield('title')</h4>
                    </div>
                    @yield('content')

{{--                    <div class="card">--}}

{{--                        <div class="card-body">--}}

{{--                        </div>--}}
{{--                    </div>--}}

                </div>
            </div>

        </div>
    @endif
    @include('common.essential_js')
@else
    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        {{--        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ url('/') }}/img/crownpaints.png">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Crown Paints Customer Service') }}</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
              type="text/css">
        <!-- App css -->
        <link href="{{ url('backend') }}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{ url('backend') }}/assets/css/icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{ url('backend') }}/assets/css/app.min.css" rel="stylesheet" type="text/css"/>
        <link href="{{ url(env('CDN_HOST').'css/fontawesome-all.css') }}" rel="stylesheet">
        <link href="{{ url('css/jquery.datetimepicker.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('sweetalert/dist/sweetalert.css') }}" rel="stylesheet">
        <link href="{{ url('printjs/src/css/print.css') }}" rel="stylesheet">
        <!--  Form Plugins css-->
        <link href="{{ url('backend') }}/assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('backend') }}/assets/libs/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('backend') }}/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('backend') }}/assets/libs/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="{{ url('backend') }}/assets/libs/select2/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/css/toastr.min.css" rel="stylesheet">

        <script src="{{url('/')}}/js/toastr.min.js"></script>
        <!-- /core JS files -->
        <link rel="stylesheet"
              href="{{url('/')}}/css/fine-uploader-gallery.css"/>
        <script src="{{url('/')}}/js/fine-uploader.min.js"></script>
        @if(!Request::ajax())
        <script src="{{ url('limitless/global_assets/js/main/jquery.min.js') }}"></script>
        @endif

        @stack('header-scripts')
    </head>
    <!-- /head -->

    <body>

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        <div class="navbar-custom">
            <ul class="list-unstyled topnav-menu float-right mb-0">

                <li class="d-none d-sm-block">
                    <form class="app-search">
                        <div class="app-search-box">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search...">
                                <div class="input-group-append">
                                    <button class="btn" type="submit">
                                        <i class="fe-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </li>



                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown"
                       href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false">
                        <i class="fe-bell noti-icon"></i>
                        <span class="badge badge-danger rounded-circle noti-icon-badge">4</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="m-0">
                                    <span class="float-right">
                                        <a href="javascript:void(0)" class="text-dark">
                                            <small>Clear All</small>
                                        </a>
                                    </span>Notification
                            </h5>
                        </div>

                        <div class="slimscroll noti-scroll">

                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                <div class="notify-icon bg-soft-primary text-primary">
                                    <i class="mdi mdi-comment-account-outline"></i>
                                </div>
                                <p class="notify-details">Doug Dukes commented on Admin Dashboard
                                    <small class="text-muted">1 min ago</small>
                                </p>
                            </a>





                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <div class="notify-icon bg-soft-warning text-warning">
                                    <i class="mdi mdi-account-plus"></i>
                                </div>
                                <p class="notify-details">New user registered.
                                    <small class="text-muted">5 hours ago</small>
                                </p>
                            </a>

                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <div class="notify-icon bg-info">
                                    <i class="mdi mdi-comment-account-outline"></i>
                                </div>
                                <p class="notify-details">Caleb Flakelar commented on Admin
                                    <small class="text-muted">4 days ago</small>
                                </p>
                            </a>

                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <div class="notify-icon bg-secondary">
                                    <i class="mdi mdi-heart"></i>
                                </div>
                                <p class="notify-details">Carlos Crouch liked
                                    <b>Admin</b>
                                    <small class="text-muted">13 days ago</small>
                                </p>
                            </a>
                        </div>

                        <!-- All-->
                        <a href="javascript:void(0);"
                           class="dropdown-item text-center text-primary notify-item notify-all">
                            View all
                            <i class="fi-arrow-right"></i>
                        </a>

                    </div>
                </li>

                <li class="dropdown notification-list">
                    <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown"
                       href="pages-starter.html#" role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{ url('/') }}/img/user.svg" height="32" width="32" alt="user-image"
                             class="rounded-circle">
                        <span class="pro-user-name ml-1">
                                {{ Auth::user()->name }} <i class="mdi mdi-chevron-down"></i>
                            </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                        <!-- item-->
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome !</h6>
                        </div>

                        <!-- item-->
                        <a href="{{ url('user/profile') }}" class="dropdown-item notify-item load-page">
                            <i class="remixicon-account-circle-line"></i>
                            <span>My Account</span>
                        </a>

                        <div class="dropdown-divider"></div>

                        <!-- item-->
                        <a href="javascript:void(0)" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                           class="dropdown-item notify-item">
                            <i class="remixicon-logout-box-line"></i>
                            <span>{{ __('Logout') }}</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </div>
                </li>


            </ul>

            <!-- LOGO -->
            <div class="logo-box">
                <a href="index.html" class="logo text-center">
                        <span class="logo-lg">
                            <img src="{{ url('/') }}/img/crownpaints.png" height="55" alt="">
{{--                             <span class="logo-lg-text-light">Xeria</span>--}}
                        </span>
                    <span class="logo-sm">
                            <!-- <span class="logo-sm-text-dark">X</span> -->
                            <img src="{{ url('/') }}/img/crownpaints.png" alt="" height="24">
                        </span>
                </a>
            </div>

            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <li>
                    <button class="button-menu-mobile waves-effect waves-light">
                        <i class="fe-menu"></i>
                    </button>
                </li>

                <li class="dropdown d-none d-lg-block">
                    <a class="nav-link dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
                       href="pages-starter.html#" role="button" aria-haspopup="false" aria-expanded="false">
                        Create New
                        <i class="mdi mdi-chevron-down"></i>
                    </a>
                    <div class="dropdown-menu">
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">
                            <i class="fe-briefcase mr-1"></i>
                            <span>New Projects</span>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">
                            <i class="fe-user mr-1"></i>
                            <span>Create Users</span>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">
                            <i class="fe-bar-chart-line- mr-1"></i>
                            <span>Revenue Report</span>
                        </a>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">
                            <i class="fe-settings mr-1"></i>
                            <span>Settings</span>
                        </a>

                        <div class="dropdown-divider"></div>

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">
                            <i class="fe-headphones mr-1"></i>
                            <span>Help & Support</span>
                        </a>

                    </div>
                </li>


            </ul>
        </div>
        <!-- end Topbar -->

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu">

            <div class="slimscroll-menu">

                <!--- Sidemenu -->
                <div id="sidebar-menu">

                    <ul class="metismenu" id="side-menu">

                        <li class="menu-title">Navigation</li>

                        @if(isset($real_menus))
                            @foreach($real_menus as $menu)

                                @if($menu->type=='single' && @$menu->menus)
                                    <li class="nav-item">
                                        <a href="{{ url($menu->menus->url)  }}" class="waves-effect load-page">
                                            <i class="fa fa-{{ $menu->menus->icon }}"></i>
                                            <span>{{ $menu->menus->label }}</span>
                                        </a>
                                    </li>

                                @endif

                                @if($menu->type=='many')
                                    <li>
                                        <a href="javascript: void(0);" class="waves-effect">
                                            <i class="fa fa-{{ $menu->icon }}"></i>
                                            <span> {{ $menu->label }} </span>
                                            <span class="menu-arrow"></span>
                                        </a>
                                        <ul class="nav-second-level" aria-expanded="false">
                                            @foreach($menu->children as $drop)
                                                @if($drop->label)
                                                    <li>
                                                        <a href="{{ url($drop->url) }}"
                                                           class="load-page">{{ $drop->label }}</a>
                                                    </li>
                                                @endif
                                            @endforeach

                                        </ul>
                                    </li>
                                @endif

                            @endforeach
                        @endif

                    </ul>

                </div>
                <!-- End Sidebar -->

                <div class="clearfix"></div>

            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content system-container">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">

                            <div class="page-title-box">
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a class="load-page"
                                                                       href="{{ url(Auth::user()->role) }}">Home</a>
                                        </li>
                                        @yield('bread_crumb')
                                        &nbsp;&nbsp;&nbsp;
                                        <a data-toggle="tooltip" title="Click to Reload Page"
                                           href="{{ url(\Illuminate\Support\Facades\Request::fullUrl()) }}"
                                           class=" load-page" data-action="reload"><i
                                                class="font-weight-bold remixicon-refresh-line icon-2x"></i>
                                        </a>
                                    </ol>
                                </div>
                                <h4 class="page-title system-title">@yield('title')</h4>
                            </div>


                                    @yield('content')


                        </div>
                    </div>

                </div>

            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        &copy; {{ Date('Y') }} A product of <a href="http://calltronix.co.ke/" target="_blank">Calltonix
                            Limited</a>
                    </div>
                    <div class="col-md-6">
                        <div class="text-md-right footer-links d-none d-sm-block">
                            <a href="javascript:void(0);">About Us</a>
                            <a href="javascript:void(0);">Help</a>
                            <a href="javascript:void(0);">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->
    </div>
    <!-- END wrapper -->

    <!-- Vendor js -->
    <script src="{{ url('backend') }}/assets/js/vendor.min.js"></script>
    <script src="{{ url('backend') }}/assets/libs/jquery-knob/jquery.knob.min.js"></script>
    <script src="{{ url('backend') }}/assets/libs/select2/select2.min.js"></script>

    <script src="{{ url('backend') }}/assets/libs/jquery-knob/jquery.knob.min.js"></script>
    <script src="{{ url('backend') }}/assets/libs/peity/jquery.peity.min.js"></script>

    <!-- Sparkline charts -->
    <script src="{{ url('backend') }}/assets/libs/jquery-sparkline/jquery.sparkline.min.js"></script>

    <!-- init js -->
    <script src="{{ url('backend') }}/assets/js/pages/dashboard-1.init.js"></script>
    {{--<script src="{{ url(env('CDN_HOST').'js/fontawesome.min.js') }}"></script>--}}
    <script src="{{ asset('js/jquery.history.js') }}"></script>
    <script src="{{ url('js/jquery.datetimepicker.js') }}"></script>
    <script src="{{ url('js/jquery.form.js') }}"></script>
    <!-- Form Plugins js-->
    <script src="{{ url('backend') }}/assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="{{ url('backend') }}/assets/libs/clockpicker/bootstrap-clockpicker.min.js"></script>
    <script src="{{ url('backend') }}/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="{{ url('backend') }}/assets/libs/moment/moment.min.js"></script>
    <script src="{{ url('backend') }}/assets/libs/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Init js-->
    <!-- App js -->
    <script src="{{ url('backend') }}/assets/js/app.min.js"></script>
    <script type="text/javascript">



    </script>
    @include('common.javascript')
    @stack('footer-scripts')
    <!-- Custom main Js -->
    <input type="hidden" name="material_page_loaded" value="1">
    <script src="{{ url('sweetalert/dist/sweetalert.min.js') }}"></script>

    </body>
    </html>
@endif
